<?php

require_once __DIR__ . '/vendor/autoload.php';

$request = Symfony\Component\HttpFoundation\Request::createFromGlobals();

$guru = new MattyRad\Guru($request);

$guru->touchSession();

if ($request->getMethod() === 'POST') {
    $guru->calculate();
}

require_once __DIR__ . '/template.php';
