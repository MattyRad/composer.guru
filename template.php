<!DOCTYPE html>
<html>
<head>
  <title>Composer Guru - Translate constraints to ranges</title>
  <link href="data:image/x-icon;base64,AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAqJR9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABEREQAAAAAREREREQAAAREREREREAARERERABERABERERAAAREBEQABEQABEREQAAAREQERERAAABERAQEREQAAEREAARERERAREQAREREREBEREREQEREQEBEREQARERAAERERAAEREAERERAAABERERERAAAAABEREQAAD4HwAA4AcAAMADAACAAQAAgAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIABAACAAQAAwAMAAOAHAAD4HwAA" rel="icon" type="image/x-icon" />
  <meta name="description" content="Translate composer version constraints to satisfiable ranges">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>@charset "UTF-8";body{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif;line-height:1.4;max-width:800px;margin:20px auto;padding:0 10px;color:#dbdbdb;background:#202b38;text-rendering:optimizeLegibility}button,input,textarea{transition:background-color .1s linear,border-color .1s linear,color .1s linear,box-shadow .1s linear,transform .1s ease}h1{font-size:2.2em;margin-top:0}h1,h2,h3,h4,h5,h6{margin-bottom:12px}h1,h2,h3,h4,h5,h6,strong{color:#fff}b,h1,h2,h3,h4,h5,h6,strong,th{font-weight:600}blockquote{border-left:4px solid rgba(0,150,191,.67);margin:1.5em 0;padding:.5em 1em;font-style:italic}blockquote>footer{margin-top:10px;font-style:normal}address,blockquote cite{font-style:normal}a[href^=mailto]:before{content:"📧 "}a[href^=tel]:before{content:"📞 "}a[href^=sms]:before{content:"💬 "}button,input[type=button],input[type=checkbox],input[type=submit]{cursor:pointer}input:not([type=checkbox]):not([type=radio]),select{display:block}button,input,select,textarea{color:#fff;background-color:#161f27;font-family:inherit;font-size:inherit;margin-right:6px;margin-bottom:6px;padding:10px;border:none;border-radius:6px;outline:none}button,input:not([type=checkbox]):not([type=radio]),select,textarea{-webkit-appearance:none}textarea{margin-right:0;width:100%;box-sizing:border-box;resize:vertical}button,input[type=button],input[type=submit]{padding-right:30px;padding-left:30px}button:hover,input[type=button]:hover,input[type=submit]:hover{background:#324759}button:focus,input:focus,select:focus,textarea:focus{box-shadow:0 0 0 2px rgba(0,150,191,.67)}button:active,input[type=button]:active,input[type=checkbox]:active,input[type=radio]:active,input[type=submit]:active{transform:translateY(2px)}button:disabled,input:disabled,select:disabled,textarea:disabled{cursor:not-allowed;opacity:.5}::-webkit-input-placeholder{color:#a9a9a9}:-ms-input-placeholder{color:#a9a9a9}::-ms-input-placeholder{color:#a9a9a9}::placeholder{color:#a9a9a9}a{text-decoration:none;color:#41adff}a:hover{text-decoration:underline}code,kbd{background:#161f27;color:#ffbe85;padding:5px;border-radius:6px}pre>code{padding:10px;display:block;overflow-x:auto}img{max-width:100%}hr{border:none;border-top:1px solid #dbdbdb}table{border-collapse:collapse;margin-bottom:10px;width:100%}td,th{padding:6px;text-align:left}th{border-bottom:1px solid #dbdbdb}tbody tr:nth-child(2n){background-color:#161f27}::-webkit-scrollbar{height:10px;width:10px}::-webkit-scrollbar-track{background:#161f27;border-radius:6px}::-webkit-scrollbar-thumb{background:#324759;border-radius:6px}::-webkit-scrollbar-thumb:hover{background:#415c73}</style>
</head>
<body>
  <div style="text-align: center;">
    <h1>Composer semver calculator</h1>
    <?php if (! $history = $request->getSession()->get('history')): ?>
      <img src="https://getcomposer.org/img/logo-composer-transparent<?= rand(2,5) ?>.png">
    <?php endif; ?>
  </div>
  <hr>
  <form method="POST" action="/">
    <div style="text-align: center; display: flex; justify-content: center;">
      <p>Enter your version constraint to get a range of satisfiable versions. See the <a href="https://getcomposer.org/doc/articles/versions.md#writing-version-constraints">docs</a> for finer details.</p>
    </div>
    <div style="text-align: center; display: flex; justify-content: center;">
      <input id="vc" type="text" name="vc" placeholder="^2.1 || ~4.0" value="">
      <button type="submit">Translate</button>
    </div>
    <div style="text-align: center; display: flex; justify-content: center;">
      <?php if ($errors = $request->getSession()->getFlashBag()->get('errors')): ?>
        <?php foreach ($errors as $error): ?>
          <p style="color: red; opacity: 0.7;"><?= $error ?></p>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>
  </form>
  <?php if ($history): ?>
    <br>
    <table>
      <thead>
        <tr>
          <th>Constraint</th>
          <th>Matches</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($history as $translation): ?>
          <?php if (isset($translation['ranges'])): ?>
            <tr>
              <td><code><?= $translation['vc'] ?? '' ?></code></td>
              <td>
                <code><?php foreach ($translation['ranges'] as $range): ?><?= str_replace('9223372036854775807.0.0.0', '&infin;', $range) ?> <?php endforeach; ?></code>
                <?php foreach (($translation['branches'] ?? []) as $branch): ?>
                  <code><?= $branch ?></code>
                <?php endforeach; ?>
              </td>
            </tr>
          <?php endif; ?>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php endif; ?>
  <footer <?php if (isset($history)): ?>style="opacity: 0.2;"<?php endif; ?>>
    <blockquote><small>Be sure to check out the semver tool at <a href="https://semver.mwl.be">semver.mwl.be</a> as well.</small></blockquote>
    <hr>
    <div style="display: flex; justify-content: space-between;">
      <time style="opacity: 0.5;">© <?= date('Y') ?></time>
      <a href="https://gitlab.com/MattyRad/composer.guru">
        <img width="50" src="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png">
      </a>
    </div>
  </footer>
</body>
</html>
