<?php

namespace MattyRad;

use Composer\Semver;
use Symfony\Component\HttpFoundation;

class Guru
{
    public function __construct(private HttpFoundation\Request $request,) {}

    public function touchSession(): void
    {
        if (! $this->request->hasSession()) {
            $session = new HttpFoundation\Session\Session;
            $session->start();
            $this->request->setSession($session);
        }
    }

    public function calculate(): void
    {
        $vc = $this->request->get('vc');

        $error = null;

        if ((! $vc) || (! is_string($vc))) {
            $vc = '^2.1 || ~4.0';
        }

        if (strlen($vc) > 40) {
            $error = 'The version constraint cannot be greater than 40 characters';
        }

        if ($error) {
            self::redirect(['errors' => $error]);
        }

        $parser = new Semver\VersionParser();

        try {
            $constraint = $parser->parseConstraints($vc);

            $intervals = Semver\Intervals::get($constraint);

            if (is_array($intervals)) {
                array_walk_recursive($intervals, function (&$c) {
                    if ($c instanceof Semver\Interval) {
                        $c = array('start' => (string) $c->getStart(), 'end' => (string) $c->getEnd());
                    }
                });
            }
        } catch (\Throwable) {
            self::redirect(['errors' => 'Invalid constraint.']);
        }

        $ranges = $intervals['numeric'] ?? [];

        $results = [];

        foreach ($ranges as $range) {
            $results[] = $range['start'] . ' ' . $range['end'];
        }

        self::pushConstraintHistory($vc, $results, $intervals['branches']['names'] ?? []);

        self::redirect();
    }

    private function redirect(array $flash_data = [], string $path = '/',): never
    {
        foreach ($flash_data as $k => $v) {
            $this->request->getSession()->getFlashBag()->add($k, $v);
        }

        $redirect = new HttpFoundation\RedirectResponse($path);
        $redirect->prepare($this->request);
        $redirect->send();
        exit;
    }

    private function pushConstraintHistory(string $vc, array $ranges, array $branches,): void
    {
        $history = $this->request->getSession()->get('history') ?: [];

        array_unshift($history, [
            'vc' => $vc,
            'ranges' => $ranges,
            'branches' => $branches,
        ]);

        if (count($history) > 5) {
            $history = array_slice($history, 0, 5);
        }

        $this->request->getSession()->set('history', $history);
    }
}
